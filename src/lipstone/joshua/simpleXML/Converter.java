package lipstone.joshua.simpleXML;

/**
 * @author Joshua Lipstone
 * @param <T>
 *            the type of object to convert
 */
public interface Converter<T> {
	
	/**
	 * This should convert the data produced by {@link #toString(Object) toString(data)} into an object of type <tt>T</tt>
	 * 
	 * @param data
	 *            the data to convert
	 * @return the object represented by this data
	 */
	public T fromString(String data);
	
	/**
	 * This should convert the data within the object produced by {@link #fromString(String) fromString(data)} into a
	 * <tt>String</tt> that can be stored in an XML file
	 * 
	 * @param data
	 *            the object to convert
	 * @return the <tt>String</tt> that represents this object.
	 */
	public String toString(T data);
}
