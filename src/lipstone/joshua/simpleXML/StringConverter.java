package lipstone.joshua.simpleXML;

/**
 * A simple one-to-one Converter for SimpleXML implementations that uses <tt>String</tt> as its data type.
 * 
 * @author Joshua Lipstone
 */
public class StringConverter implements Converter<String> {
	
	@Override
	public String fromString(String data) {
		return data;
	}
	
	@Override
	public String toString(String data) {
		return data;
	}
	
}
