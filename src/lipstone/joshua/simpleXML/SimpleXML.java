package lipstone.joshua.simpleXML;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * A simple system for saving groups of data to and loading from an XML file.
 * 
 * @author Joshua Lipstone
 * @param <T>
 *            the type of object to handle
 */
public class SimpleXML<T> implements Iterable<String> {
	private String filePath;
	private Converter<T> converter;
	private Document dom;
	private HashMap<String, HashMap<String, T>> data;
	
	/**
	 * Creates a new SimpleXML object with the data contained in the XML file at <tt>filePath</tt>
	 * 
	 * @param filePath
	 *            the path to the XML file that this <tt>SimpleXML</tt> object will load data from and save data to
	 * @param converter
	 *            the contains the needed methods to convert between the object as the program uses it and how the XML stores
	 *            it.
	 */
	public SimpleXML(String filePath, Converter<T> converter) {
		super();
		this.filePath = filePath;
		File check = new File(filePath);
		if (!check.exists()) {
			String path = filePath.substring(0, filePath.lastIndexOf('/'));
			new File(path).mkdirs();
		}
		this.converter = converter;
		data = new HashMap<>();
		loadFromXML();
	}
	
	/**
	 * Determines if the XML contains a group with the name in key
	 * 
	 * @param key
	 *            the name of the set to search for.
	 * @return true if this XML contains key
	 */
	public boolean containsKey(String key) {
		return data.containsKey(key);
	}
	
	/**
	 * Determines if the XML contains a value named by value within the group with the name in key
	 * 
	 * @param key
	 *            the name of the set to search for.
	 * @param value
	 *            the name of the value to search for within the set
	 * @return true if this XML contains key and the set denoted by key contains value
	 */
	public boolean containsValue(String key, String value) {
		return data.containsKey(key) ? data.get(key).containsKey(value) : false;
	}
	
	/**
	 * Gets the data mapped to value within the set mapped to key
	 * 
	 * @param key
	 *            the set of values to retrieve the data contained in value from
	 * @param value
	 *            the value to retrieve
	 * @return the data mapped to value within the set mapped to key if it exists, otherwise null
	 */
	public T get(String key, String value) {
		return data.containsKey(key) ? data.get(key).get(value) : null;
	}
	
	/**
	 * Maps datum to value within the set key. If value is already mapped within the set, the data previously mapped to it
	 * will be overwritten.
	 * 
	 * @param key
	 *            the data set
	 * @param value
	 *            the value within that set
	 * @param datum
	 *            the new value to put in the map
	 */
	public void put(String key, String value, T datum) {
		if (!data.containsKey(key))
			data.put(key, new HashMap<String, T>());
		data.get(key).put(value, datum);
		NodeList nl = dom.getDocumentElement().getElementsByTagName(key);
		if (nl.getLength() != 1) {
			dom.getDocumentElement().appendChild(createElement(key, data.get(key)));
			writeToXML();
			return;
		}
		Element e = (Element) nl.item(0);
		NodeList el = e.getElementsByTagName(value);
		if (el.getLength() > 0)
			for (int i = 0; i < el.getLength(); i++)
				e.removeChild(el.item(i));
		Element data = dom.createElement(value);
		data.setAttribute("data", converter.toString(datum));
		e.appendChild(data);
		writeToXML();
	}
	
	/**
	 * Removes the entire set mapped to key from the XML.
	 * 
	 * @param key
	 *            the key for the set to be removed
	 */
	public void remove(String key) {
		if (!data.containsKey(key))
			return;
		data.remove(key);
		Element e = dom.getDocumentElement();
		NodeList nl = e.getElementsByTagName(key);
		for (int i = 0; i < nl.getLength(); i++)
			e.removeChild(nl.item(i));
		writeToXML();
	}
	
	/**
	 * Removes the data mapped to value in the set mapped to key from the XML
	 * 
	 * @param key
	 *            the set to remove from
	 * @param value
	 *            the value to remove
	 * @return the data that was removed if value was mapped to something, otherwise null
	 */
	public T remove(String key, String value) {
		if (!data.containsKey(key) || !data.get(key).containsKey(value))
			return null;
		T datum = data.get(key).remove(value);
		NodeList nl = dom.getDocumentElement().getElementsByTagName(key);
		if (nl.getLength() != 1)
			return datum;
		Element e = (Element) nl.item(0);
		NodeList el = e.getElementsByTagName(value);
		for (int i = 0; i < el.getLength(); i++)
			e.removeChild(el.item(i));
		writeToXML();
		return datum;
	}
	
	private void createXML() {
		try {
			dom = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
		}
		catch (ParserConfigurationException e) {};
		createDOMTree();
	}
	
	private void createDOMTree() {
		Element rootEle = dom.createElement("simpleXML");
		dom.appendChild(rootEle);
		for (String key : data.keySet())
			rootEle.appendChild(createElement(key, data.get(key)));
	}
	
	private Element createElement(String name, HashMap<String, T> data) {
		Element e = dom.createElement(name);
		for (String key : data.keySet()) {
			Element datum = dom.createElement(key);
			datum.setAttribute("data", converter.toString(data.get(key)));
			e.appendChild(datum);
		}
		return e;
	}
	
	/**
	 * Writes the data to the XML. This is automatically called in {@link #put(String, String, Object) put(key, value,
	 * datum)}, {@link #remove(String) remove(key)}, and {@link #remove(String, String) remove(key, value)}.
	 */
	public void writeToXML() {
		if (dom.equals(null))
			createXML();
		try {
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(dom);
			StreamResult result = new StreamResult(new File(filePath));
			
			transformer.transform(source, result);
		}
		catch (TransformerException tfe) {}
	}
	
	/**
	 * Loads the data from the XML. This is automatically called by the constructor and probably should not be called after.
	 */
	public void loadFromXML() {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		File xmlData = new File(filePath);
		if (!xmlData.exists()) {
			createXML();
			writeToXML();
		}
		else {
			try {
				DocumentBuilder db = dbf.newDocumentBuilder();
				dom = db.parse(filePath);
			}
			catch (ParserConfigurationException pce) {}
			catch (SAXException se) {}
			catch (IOException ioe) {}
		}
		parseDocument();
	}
	
	private void parseDocument() {
		Element docElement = dom.getDocumentElement();
		Element docE = (Element) docElement.getFirstChild();
		if (docE == null)
			return;
		do {
			appendElement(docE);
		} while ((docE = (Element) docE.getNextSibling()) != null);
	}
	
	private void appendElement(Element e) {
		String key = e.getTagName();
		Element datum = (Element) e.getFirstChild();
		if (!data.containsKey(key))
			data.put(key, new HashMap<String, T>());
		do {
			data.get(key).put(datum.getTagName(), converter.fromString(datum.getAttribute("data")));
		} while ((datum = (Element) datum.getNextSibling()) != null);
	}
	
	@Override
	public Iterator<String> iterator() {
		return data.keySet().iterator();
	}
	
	public int size() {
		return data.size();
	}
}
